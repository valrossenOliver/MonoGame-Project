﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using MonoGame.src.Graphics;
using MonoGame.src.SceneManagement;
using System;
using System.IO;
using static MonoGame.src.Graphics.ScreenHandler;

namespace MonoGame.src
{
    public static class GameFramework
    {
        static bool _init;

        /// <summary>
        /// Initializes the framework with all static settings required for it to run.
        /// </summary>
        /// <param name="game"></param>
        /// <param name="graphics"></param>
        /// <param name="content"></param>
        /// <param name="ScreenSize"></param>
        /// <param name="screenSetting"></param>
        /// <returns></returns>
        public static bool Init(Game1 game, ref GraphicsDeviceManager graphics,
            ContentManager content, Point? ScreenSize = null,
            ScreenSettings screenSetting = ScreenSettings.Windowed)
        {
            _init = false;

            try
            {
                //Graphics
                ScreenHandler.Initialize(game, ref graphics, new Point(1920, 1080), screenSetting);
                TextHandler.Initialize(content);
                TextureHandler.Initialize(content, graphics.GraphicsDevice);
                GameConsole.Initialize(game, ref graphics);
                GameCursor.Initialize(content);

                //Scenes
                SceneManager.Initialize(game, content, graphics.GraphicsDevice);
                Scene.Initialize(game, content, graphics.GraphicsDevice);

                _init = true;
            }
            catch (Exception)
            {
                Console.WriteLine("-- FAILED LOADING FRAMEWORK --");
                File.WriteAllText("Error.log", "-- FAILED LOADING FRAMEWORK --");
            }

            return _init;
        }

        /// <summary>
        /// This is called from scene-classes within the framework
        /// in case Init() was called incorrectly or was not called at all.
        /// </summary>
        internal static void TestInitialization()
        {
            //Throw exception if initialization is not complete.
            if (_init == false)
                throw new Exception("Framework was not initalized! " +
                    "Did you call Init() during the initialization process?");
        }
    }
}
