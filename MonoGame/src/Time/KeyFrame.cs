﻿namespace MonoGame.src.Time
{
    public struct KeyFrame
    {
        float _length;
        Transform _transform;

        public float TimeSpan { get => _length; set => _length = value; }
        public Transform Transform { get => _transform; set => _transform = value; }

        public KeyFrame(float seconds, Transform transform)
        {
            _length = seconds;
            _transform = transform;
        }
    }
}
