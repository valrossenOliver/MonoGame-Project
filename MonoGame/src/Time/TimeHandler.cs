﻿namespace MonoGame.src.Time
{
    public static class TimeHandler
    {
        static float _timeScale = 1.0f;

        /// <summary>
        /// Speed at which the game runs.
        /// </summary>
        public static float TimeScale { get => _timeScale; set => _timeScale = value; }
    }
}
