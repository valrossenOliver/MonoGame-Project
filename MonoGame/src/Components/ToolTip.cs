﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.Linq;
using System;
using MonoGame.src.Graphics;

namespace MonoGame.src.Components
{
    public class ToolTip : Component, IDrawableComponent
    {
        #region Fields
        static bool A_TOOLTIP_IS_SHOWING = false;

        /// <summary>
        /// The maximum size of a tooltip.
        /// </summary>
        const float _MAX_WIDTH = 600; //TODO : Make a settings class with this.

        public enum TextColors { white, red, green, blue, yellow }

        //Graphical Settings
        Vector2 _size;
        Vector2 _position;
        Color _textColor;
        Color _backColor;

        //Text Settings
        string _rawText;
        List<string> _words;
        List<Color> _wordColor;

        List<string> _conditions;
        List<Color> _conditionColor;

        List<Condition> _lastConditionUpdate;

        //Logical Settings
        float _popupDelay;
        bool visible;
        bool _hovering;
        TimeSpan _mouseHoverTime; 
        #endregion

        #region Properties

        public string Text
        {
            get
            {
                return this.ToString();
            }
            set
            {
                _rawText = value;

                //Parse all words from the text
                Color color = _textColor;
                while (value.Length > 0)
                {
                    bool CONTINUE = true;
                    CONTINUE = ParseWords(CONTINUE, ref value, ref color,
                        ref _words, ref _wordColor);
                }
            }
        }
        public bool Visible { get => visible; } 

        #endregion

        public ToolTip()
        {
            _size = Vector2.Zero;
            _position = Vector2.Zero;
            _textColor = Color.White;
            _backColor = new Color(0, 0, 0, 0);

            _rawText = "";
            _words = new List<string>();
            _wordColor = new List<Color>();

            _conditions = new List<string>();
            _conditionColor = new List<Color>();
            _lastConditionUpdate = new List<Condition>();

            _popupDelay = 0.5f;
            _hovering = false;
            _mouseHoverTime = TimeSpan.Zero;
        }

        #region Methods

        public void SetVisibility(bool value) => visible = value;

        // ==================================================================
        //                  TEXT CHANGING METHODS
        // ==================================================================

        private bool ParseWords(bool cont, ref string text,
            ref Color color, ref List<string> wordList, ref List<Color> colorList)
        {
            string word = "";

            while (cont)
            {
                if (text.Length == 0)
                {
                    wordList.Add(word);
                    colorList.Add(color);
                    break;
                }

                if (text[0] == ' ' || text[0] == ',' || text[0] == '.' || text[0] == '!' || text[0] == '?' || text[0] == '-')
                {
                    word += text[0];
                    text = text.Remove(0, 1);

                    wordList.Add(word);
                    colorList.Add(color);

                    cont = false;
                }
                else if (text.Length >= 2)
                {
                    if (text.Substring(0, 2) == "{{") //PERFORM COMMAND..
                    {
                        cont = false;

                        //Empty Previous word
                        wordList.Add(word);
                        colorList.Add(color);
                        word = "";

                        //..Changing TextColor
                        if (text.StartsWith("{{color}}"))
                        {
                            //Take care of the command in-text
                            text = text.TrimStart("{{color}}".ToCharArray());

                            if (text[0] != '[')
                            {
                                color = _textColor;
                            }
                            else
                            {
                                string subString = "";
                                foreach (var c in text)
                                {
                                    subString += c;

                                    if (c == ']')
                                        break;
                                }

                                text = text.Remove(0, subString.Count());

                                subString = subString.Remove(0, 1);
                                subString = subString.Remove(subString.Length - 1);

                                //Change Color
                                switch (subString)
                                {
                                    case "red":
                                        color = new Color(220, 0, 0, 255);
                                        break;

                                    case "green":
                                        color = new Color(0, 250, 0, 255);
                                        break;

                                    case "yellow":
                                        color = Color.Yellow;
                                        break;

                                    case "blue":
                                        color = new Color(5, 132, 252, 255);
                                        break;

                                    default:
                                        color = _textColor;
                                        break;
                                }
                            }
                        }

                        //..New Line
                        else if (text.StartsWith("{{nl}}"))
                        {
                            wordList.Add("{{nl}}");
                            colorList.Add(color);

                            text = text.Remove(0, "{{nl}}".Count());
                        }

                        //Invalid input
                        else
                        {
                            Console.WriteLine("WARNING :: Incomplete command!");

                            word += "{{";

                            wordList.Add(word);
                            colorList.Add(color);

                            text = text.Remove(0, 2);

                            cont = false;
                        }
                    }
                    else
                    {
                        word += text[0];
                        text = text.Remove(0, 1);
                    }
                }
                else if (text.Length == 0)
                {
                    wordList.Add(word);
                    colorList.Add(color);
                    break;
                }
                else
                {
                    word += text[0];
                    text = text.Remove(0, 1);
                }
            }

            return cont;
        }

        public void UpdateConditions(List<Condition> conditions)
        {
            _lastConditionUpdate = conditions.ToList();

            if (conditions.Count > 0)
            {
                //Empty previous conditions
                _conditions = new List<string>();
                _conditionColor = new List<Color>();

                //Seperator between tooltip and conditions
                string text = "";

                if (_words.Count > 0)
                {
                    while (TextHandler.Default.MeasureString(text + "-").X < this._size.X || text.Length < 30)
                    {
                        text += "-";
                    }

                    _conditions.Add(text);
                    _conditionColor.Add(new Color(_textColor.ToVector4()));
                    text = "";
                }

                text = ExtractConditionText(conditions, text);

                //Parse all words from the text
                Color color = _textColor;
                while (text.Length > 0)
                {
                    bool CONTINUE = true;
                    CONTINUE = ParseWords(CONTINUE, ref text, ref color, ref _conditions, ref _conditionColor);
                }
            }
        }

        private static string ExtractConditionText(List<Condition> conditions, string text,
            int intentation = 0)
        {
            foreach (var condition in conditions)
            {
                if (!(intentation == 0 && text == ""))
                {
                    text += "{{nl}}";
                }

                for (int i = 0; i < intentation * 4; i++)
                {
                    if (i == intentation * 4 - 2)
                    {
                        text += "|";
                    }
                    else
                    text += " ";
                }

                text += condition.Description + " ({{color}}";

                if (condition.Fullfilled == true)
                {
                    text += "[green]\u221A";
                }
                else
                {
                    text += "[red]x";
                }

                text += "{{color}}[*])";

                if (condition.SubConditions.Count > 0)
                {
                    text += ExtractConditionText(condition.SubConditions, "", intentation + 1);
                }
            }

            return text;
        }

        // ==================================================================
        //                      UPDATE METHODS
        // ==================================================================

        private void CalculateSize()
        {
            float total_width = 0;
            float total_height = 0;

            string line = "";
            int count = 0;

            foreach (var word in _words)
            {
                count++;
                var linewidth = TextHandler.Default.MeasureString(line).X;
                var wordwidth = TextHandler.Default.MeasureString(word).X;
                var total = TextHandler.Default.MeasureString(string.Concat(line, word)).X;
                if (TextHandler.Default.MeasureString(string.Concat(line, word)).X <= _MAX_WIDTH && word != "{{nl}}")
                {
                    line += word;
                }
                else
                {
                    if (TextHandler.Default.MeasureString(line).X > total_width)
                    {
                        total_width = TextHandler.Default.MeasureString(line).X;
                    }

                    if (line == "{{nl}}")
                    {
                        line = " ";
                    }

                    total_height += TextHandler.Default.MeasureString(line).Y;

                    line = "";

                    if (word != "{{nl}}")
                        line += word;
                }

                if (count == _words.Count)
                {
                    if (TextHandler.Default.MeasureString(line).X > total_width)
                    {
                        total_width = TextHandler.Default.MeasureString(line).X;
                    }

                    total_height += TextHandler.Default.MeasureString(line).Y;

                    line = "";
                }
            }

            count = 0;
            foreach (var word in _conditions)
            {
                count++;

                if (TextHandler.Default.MeasureString(string.Concat(line, word)).X <= _MAX_WIDTH && word != "{{nl}}")
                {
                    line += word;
                }
                else
                {
                    if (TextHandler.Default.MeasureString(line).X > total_width)
                    {
                        total_width = TextHandler.Default.MeasureString(line).X;
                    }

                    if (line == "{{nl}}")
                    {
                        line = " ";
                    }

                    total_height += TextHandler.Default.MeasureString(line).Y;

                    line = "";
                    line += word;
                }

                if (count == _conditions.Count)
                {
                    if (TextHandler.Default.MeasureString(line).X > total_width)
                    {
                        total_width = TextHandler.Default.MeasureString(line).X;
                    }

                    total_height += TextHandler.Default.MeasureString(line).Y;

                    line = "";
                }
            }

            bool updateToolTip = false;

            if (_size.X != total_width)
            {
                updateToolTip = true;
            }

            _size = new Vector2(total_width, total_height);

            if (updateToolTip)
                UpdateConditions(_lastConditionUpdate);
        }

        private void CalculatePosition()
        {
            _position = Mouse.GetState().Position.ToVector2()
                + new Vector2(20, 30);

            if (_position.X + _size.X > ScreenHandler.Width - 5)
            {
                _position.X = Mouse.GetState().Position.ToVector2().X
                    - _size.X - 10;
            }
            if (_position.Y + _size.Y > ScreenHandler.Height - 5)
            {
                _position.Y = Mouse.GetState().Position.ToVector2().Y
                    - _size.Y - 10;
            }
        }

        private void CalculateDelay(GameTime gameTime)
        {
            if (_hovering && !Visible)
            {
                _mouseHoverTime = _mouseHoverTime.Add(gameTime.ElapsedGameTime);

                if (_mouseHoverTime.TotalMilliseconds > _popupDelay * 1000)
                {
                    SetVisibility(true);
                    A_TOOLTIP_IS_SHOWING = true;
                    _mouseHoverTime = TimeSpan.Zero;
                }
            }
            else if (!_hovering && Visible)
            {
                SetVisibility(false);
                A_TOOLTIP_IS_SHOWING = false;

                _backColor.A = 10;
            }

            if (Visible)
            {
                CalculateSize();
                CalculatePosition();

                if (_backColor.A < 200)
                {
                    _backColor.A += 8;
                }
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (A_TOOLTIP_IS_SHOWING == false || Visible)
            {
                CalculateDelay(gameTime);

                var collider = gameObject.GetComponent<BoxCollider>() as BoxCollider;

                if (collider != null && collider.Contains(Mouse.GetState().Position))
                {
                    _hovering = true;
                }
                else _hovering = false;
            }
            else
            {
                _mouseHoverTime = TimeSpan.Zero;
                _hovering = false;
            }
        }

        // ==================================================================
        //                      DRAWING METHODS
        // ==================================================================

        private void DrawText(SpriteBatch spriteBatch)
        {
            Vector2 Offset = Vector2.Zero;

            //Text
            for (int count = 0; count < _words.Count; count++)
            {
                Vector2 word_size = TextHandler.Default.MeasureString(_words[count]);
                var x = _words[count];

                if (_words[count] == "{{nl}}")
                {
                    Offset.Y += word_size.Y;
                    Offset.X = 0;
                }
                else
                {
                    if (word_size.X + Offset.X > _MAX_WIDTH)
                    {
                        Offset.X = 0;
                        Offset.Y += word_size.Y;
                    }

                    spriteBatch.DrawString(TextHandler.Default, _words[count],
                        _position + Offset, _wordColor[count], 0,
                        Vector2.Zero, 1, SpriteEffects.None, 1f);

                    Offset.X += word_size.X;
                }
            }

            if (_words.Count > 0)
                Offset += new Vector2(0, TextHandler.Default.MeasureString("{nl}").Y);

            Offset.X = 0;

            //Conditions
            for (int count = 0; count < _conditions.Count; count++)
            {
                Vector2 word_size = TextHandler.Default.MeasureString(_conditions[count]);

                if (_conditions[count] == "{{nl}}")
                {
                    Offset.Y += word_size.Y;
                    Offset.X = 0;
                }
                else
                {
                    if (word_size.X + Offset.X > _MAX_WIDTH)
                    {
                        Offset.X = 0;
                        Offset.Y += word_size.Y;
                    }

                    spriteBatch.DrawString(TextHandler.Default, _conditions[count],
                        _position + Offset, _conditionColor[count], 0,
                        Vector2.Zero, 1, SpriteEffects.None, 1f);

                    Offset.X += word_size.X;
                }
            }
        }

        private void DrawBackground(SpriteBatch spriteBatch)
        {
            Rectangle Background = new Rectangle(_position.ToPoint(), _size.ToPoint());

            Background.Location -= new Point(5);
            Background.Size += new Point(10);

            spriteBatch.Draw(TextureHandler.Pixel, Background,
                null, _backColor, 0, Vector2.Zero, SpriteEffects.None, 0.99f);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Visible && (_words.Count > 0 || _conditions.Count > 0))
            {
                //BACKGROUND
                DrawBackground(spriteBatch);

                //TEXT
                DrawText(spriteBatch);
            }
        }

        public override string ToString()
        {
            string text = "";

            foreach (var word in _words)
            {
                if (word != "{{nl}}") { text += word; }
            }

            return text;
        }

        public override void PostUpdate(GameTime gameTime)
        {
            
        }

        #endregion
    }
}