﻿using Microsoft.Xna.Framework;
using MonoGame.src.Time;

namespace MonoGame.src.Components
{
    public class Animator : Component
    {
        Animation _animation; //TODO : Animator can have several animations
        bool _timeBased;

        public Animation Animation { get => _animation; set => _animation = value; }
        public bool TimeBased { get => _timeBased; set => _timeBased = value; }

        public Animator()
        {
            _animation = new Animation();
            _animation.OnFrame += OnFrame_MoveGameObject;

            TimeBased = true;
        }

        private void OnFrame_MoveGameObject(Transform transform)
        {
            gameObject.Move(transform.Position, isTimeBased: TimeBased);
        }

        public override void Update(GameTime gameTime)
        {
            _animation.Update(gameTime);
        }

        public override void PostUpdate(GameTime gameTime)
        {
            
        }
    }
}
