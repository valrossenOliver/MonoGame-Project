﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGame.src.Components
{
    public abstract class Component
    {
        private bool enabled = true;
        private GameObject gameobj = null;

        public bool Enabled { get => enabled; }
        public GameObject gameObject { get => gameobj; }

        #region Methods

        public void Attach(GameObject _gameObject)
        {
            if (gameobj == null)
            {
                gameobj = _gameObject;
                OnAttach(this);
            }
        }

        protected virtual void OnAttach(Component sender)
        {   }

        #endregion

        #region GameLoop
        public abstract void Update(GameTime gameTime);
        public abstract void PostUpdate(GameTime gameTime); 
        #endregion
    }

    public interface IDrawableComponent
    {
        void Draw(GameTime gameTime, SpriteBatch spriteBatch);
    }
}