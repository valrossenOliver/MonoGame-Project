﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.src;
using MonoGame.src.Components;
using MonoGame.src.Graphics;
using System.Collections.Generic;

namespace MonoGame.src.Components
{
    public class BoxCollider : Component, IDrawableComponent
    {
        #region Fields

        Vector2 position;
        Vector2 size;
        Vector2 scale;
        bool mouseOver;

        public static List<GameObject> Active = new List<GameObject>();

        public static void DisplayDebugRectangle(bool value) => _DISPLAY_DEBUG_RECTANGLE_ = value;
        static bool _DISPLAY_DEBUG_RECTANGLE_ = false; //TODO : Massive frame-drop when active with scene containing larger amounts of boxcolliders.

        #endregion

        #region Properties

        /// <summary>
        /// Returns the components world position.
        /// </summary>
        public Vector2 Position { get => gameObject.transform.Position + LocalPosition;
            set => LocalPosition = gameObject.transform.Position - value; }

        /// <summary>
        /// Retuns the components local position compared to its gameObject.
        /// </summary>
        public Vector2 LocalPosition
        {
            get => position;
            set => position = value;
        }

        /// <summary>
        /// Returns a center-point of the collider rectangle.
        /// </summary>
        public Vector2 Center { get => Size / 2; }

        /// <summary>
        /// Returs or sets component Width and Height via a vector. (Scale * gameObject.transform.Size)
        /// </summary>
        public Vector2 Size { get => size; set => size = value; }

        /// <summary>
        /// Returns or sets the components scale (compared to its object)
        /// </summary>
        public Vector2 Scale { get => scale; set => scale = value; }

        #endregion

        public BoxCollider()
        {
            position = Vector2.Zero;
            size = new Vector2(10);
            scale = new Vector2(1);

            mouseOver = false;
        }

        protected override void OnAttach(Component sender)
        {
            Size = gameObject.transform.Size;
            Position = gameObject.transform.Position;
        }

        #region Methods

        public bool Contains(Vector2 vector)
        {
            if (Position.X <= vector.X && Position.X + Size.X >= vector.X &&
                Position.Y <= vector.Y && Position.Y + Size.Y >= vector.Y)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool Contains(Point point)
        {
            return Contains(point.ToVector2());
        }
        public bool Touch(BoxCollider other)
        {
            if (Contains(other.Position) ||
                Contains(other.Position + other.Size) ||
                Contains(other.Position + new Vector2(other.Size.X, 0)) ||
                Contains(other.Position + new Vector2(0, other.Size.Y)) ||

                other.Contains(this.Position) ||
                other.Contains(this.Position + this.Size) ||
                other.Contains(this.Position + new Vector2(this.Size.X, 0)) ||
                other.Contains(this.Position + new Vector2(0, this.Size.Y)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return "{" + "X:" + Position.X + " Y:" + Position.Y +
                " W:" + Size.X + " H:" + Size.Y + " A:" + mouseOver + "}";
        }

        #endregion

        #region GameLoop

        public override void Update(GameTime gameTime)
        {
            if (Contains(Mouse.GetState().Position.ToVector2()))
            {
                mouseOver = true;

                if (Active.Contains(gameObject) == false)
                    Active.Add(gameObject);
            }
            else
            {
                mouseOver = false;

                Active.Remove(gameObject);
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (_DISPLAY_DEBUG_RECTANGLE_)
            {
                Shape Box = new Shape();
                Box.AddPoints(new List<Vector2>
                {
                    Position,
                    Position + new Vector2(Size.X, 0),
                    Position + Size,
                    Position + new Vector2(0, Size.Y),
                    Position,
                });

                Pen p = new Pen(spriteBatch)
                {
                    Color = mouseOver ? Mouse.GetState().LeftButton == ButtonState.Pressed ? Color.Red : Color.Blue : Color.Green
                };

                p.Draw(Box);
            }
        }

        public override void PostUpdate(GameTime gameTime)
        {
            
        }

        #endregion
    }
}
