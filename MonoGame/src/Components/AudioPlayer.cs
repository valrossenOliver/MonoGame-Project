﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using MonoGame.src.Graphics;

namespace MonoGame.src.Components
{
    class AudioPlayer : Component
    {
        Dictionary<string, SoundEffect> effects;
        Dictionary<string, Song> songs;

        string songToPlay = "";

        public AudioPlayer()
        {
            effects = new Dictionary<string, SoundEffect>();
            songs = new Dictionary<string, Song>();
        }

        public void AddEffect(ContentManager content, string name)
        {
            if (effects.ContainsKey(name) == false)
            {
                var sound = content.Load<SoundEffect>("Sounds/" + name);
                effects.Add(name, sound);
            }
        }

        public void PlayEffect(string name)
        {
            if (effects.ContainsKey(name))
            {
                effects[name].Play();
            }
            else GameConsole.PrintLine(string.Format("No such effect '{0}'", name));
        }

        public void AddSong(ContentManager content, string name)
        {
            if (songs.ContainsKey(name) == false)
                songs.Add(name, content.Load<Song>("Music/" + name));
        }

        public void PlaySong(string name)
        {
            if (songs.ContainsKey(name))
            {
                songToPlay = name;
            }
            else GameConsole.PrintLine(string.Format("No such song '{0}'", name));
        }

        public override void Update(GameTime gameTime)
        {
            
        }

        public override void PostUpdate(GameTime gameTime)
        {
            if (songToPlay != "")
            {
                MediaPlayer.Play(songs[songToPlay]);
                songToPlay = "";
            }
        }
    }
}
