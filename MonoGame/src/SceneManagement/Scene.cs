﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.src.Components;
using System.Collections.Generic;
using System.Linq;

namespace MonoGame.src.SceneManagement
{
    public abstract class Scene
    {
        #region Fields
        private string name;
        private bool started;
        private Color backgroundColor = Color.RoyalBlue;
        private List<GameObject> gameObjects = new List<GameObject>();
        private List<GameObject> newObjects = new List<GameObject>();

        public string Name { get => name; }
        public bool IsStarted { get => started; }
        public Color BackgroundColor { get => backgroundColor; set => backgroundColor = value; }
        public ref List<GameObject> SceneObjects { get => ref gameObjects; }
        #endregion

        #region Properties
        static protected Game1 game;
        static protected ContentManager content;
        static protected GraphicsDevice graphics; 
        #endregion

        #region Methods
        public static void Initialize(Game1 _game, ContentManager contentManager,
            GraphicsDevice graphicsDevice)
        {
            game = _game;
            content = contentManager;
            graphics = graphicsDevice;
        }

        public Scene(string _name)
        {
            GameFramework.TestInitialization();

            name = _name;
            started = false;
        }

        public void AddObject(GameObject gameObject)
        {
            newObjects.Add(gameObject);
        }
        public void AddObjectsRange(List<GameObject> list)
        {
            newObjects.AddRange(list);
        }

        public GameObject GetObjectByName(string targetName)
        {
            foreach (var gameObject in gameObjects)
            {
                if (gameObject.Name == targetName)
                {
                    return gameObject;
                }
            }

            return null;
        }
        public List<GameObject> GetObjectsByName(string targetName)
        {
            List<GameObject> list = new List<GameObject>();

            if (targetName.Contains('%'))
            {
                var targetSearch = targetName.Split('%');

                foreach (var gameObject in gameObjects)
                {
                    bool startsWith = false;
                    bool endsWith = false;
                    bool found = false;

                    if (string.IsNullOrEmpty(targetSearch[0]) == false)
                    {
                        startsWith = true;
                    }
                    if (string.IsNullOrEmpty(targetSearch[1]) == false)
                    {
                        endsWith = true;
                    }

                    if (startsWith)
                    {
                        if (gameObject.Name.StartsWith(targetSearch[0]))
                        {
                            found = true;
                        }
                        else found = false;
                    }
                    if (endsWith && (!startsWith || (startsWith && found)))
                    {
                        if (gameObject.Name.EndsWith(targetSearch[1]))
                        {
                            found = true;
                        }
                        else found = false;
                    }

                    if (found)
                    {
                        list.Add(gameObject);
                    }
                }
            }
            else
            {
                foreach (var gameObject in gameObjects)
                {
                    if (gameObject.Name == targetName)
                    {
                        list.Add(gameObject);
                    }
                }
            }

            return list;
        }

        public GameObject GetObjectByTag(string targetTag)
        {
            foreach (var gameObject in gameObjects)
            {
                if (gameObject.Tag == targetTag)
                {
                    return gameObject;
                }
            }

            return null;
        }
        public List<GameObject> GetObjectsByTag(string targetTag)
        {
            List<GameObject> list = new List<GameObject>();

            foreach (var gameObject in gameObjects)
            {
                if (gameObject.Tag == targetTag)
                {
                    list.Add(gameObject);
                }
            }

            return list;
        } 
        #endregion

        #region Game Loop
        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            DrawObjects(gameTime, spriteBatch);
        }

        public virtual void PostUpdate(GameTime gameTime)
        {
            gameObjects.AddRange(newObjects);
            newObjects.Clear();

            PostUpateObjects(gameTime);
        }

        public virtual void Start()
        {
            started = true;

            gameObjects.AddRange(newObjects);
            newObjects.Clear();
            StartScripts();
        }

        public virtual void Reset()
        {
        }

        public virtual void Update(GameTime gameTime)
        {
            if (started)
            {
                UpdateObjects(gameTime);
            }
        }

        protected void StartScripts()
        {
            if (started)
            {
                foreach (var item in SceneObjects)
                {
                    if (item.Enabled && item.HasComponent<GameBehaviour>())
                    {
                        foreach (var script in item.GetComponents<GameBehaviour>())
                        {
                            script.Start();
                        }
                    }
                } 
            }
        }

        protected void DrawObjects(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (started)
            {
                foreach (var item in SceneObjects)
                {
                    if (item.Enabled)
                    {
                        item.Draw(gameTime, spriteBatch);
                    }
                } 
            }
        }

        protected void UpdateObjects(GameTime gameTime)
        {
            if (started)
            {
                foreach (var item in SceneObjects)
                {
                    if (item.Enabled)
                    {
                        item.Update(gameTime);
                    }
                } 
            }
        }

        protected void PostUpateObjects(GameTime gameTime)
        {
            if (started)
            {
                foreach (var item in SceneObjects.ToList())
                {
                    if (item.Enabled)
                    {
                        item.PostUpdate(gameTime);
                    }

                    if (item.Destroyed)
                    {
                        SceneObjects.Remove(item);
                    }
                } 
            }
        } 
        #endregion
    }
}
