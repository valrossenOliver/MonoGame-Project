﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame.src.Graphics
{
    static class GameCursor
    {
        static Texture2D normal;
        static Texture2D active;

        static int activeState = 0;
        static TimeSpan last_update_time;

        public static bool Active { get; set; }

        public static void Initialize(ContentManager content)
        {
            normal = content.Load<Texture2D>("Images/Cursor_Select");
            active = content.Load<Texture2D>("Images/Cursor_Active");

            Active = false;
        }

        public static void Update(GameTime gameTime)
        {
            if (Active)
            {
                var timeElapsed = gameTime.TotalGameTime.TotalSeconds - last_update_time.TotalSeconds;

                if (timeElapsed > 0.5f)
                {
                    last_update_time = gameTime.TotalGameTime;

                    if (activeState < 3) activeState++;
                    else activeState = 0;
                }

                Active = false;
            }
        }

        public static void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Active)
            {
                spriteBatch.Draw(active, new Rectangle(Mouse.GetState().Position, new Point(60)), new Rectangle(new Point(60 * activeState, 0), new Point(60, 60)), Color.White, 0, new Vector2(25, 40), SpriteEffects.None, 1);
            }
            else
            {
                spriteBatch.Draw(normal, Mouse.GetState().Position.ToVector2(), null, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 1);
            }
        }
    }
}
