﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGame.src.Graphics
{
    public class Pen
    {
        public enum DrawModes { Solid, Striped, Dotted }

        Color _color = Color.Black;
        SpriteBatch _spriteBatch = null;
        float _size = 2;

        public Color Color { get => _color; set => _color = value; }
        public float Size { get => _size; set => _size = value; }

        public Pen(SpriteBatch spriteBatch)
        {
            _spriteBatch = spriteBatch;
        }

        public void Draw(Shape shape, DrawModes drawMode = DrawModes.Solid)
        {
            bool hasPrevious = false;
            Vector2 previous = Vector2.Zero;

            foreach (var point in shape.Points)
            {
                if (hasPrevious)
                {
                    DrawLine(previous, point);
                }
                else hasPrevious = true;

                previous = point;
            }
        }

        public void DrawLine(Point point1, Point point2, DrawModes drawMode = DrawModes.Solid)
            => DrawLine(point1.ToVector2(), point2.ToVector2(), drawMode);

        public void DrawLine(Vector2 point1, Vector2 point2, DrawModes drawMode = DrawModes.Solid)
        {
            if (point1.X > point2.X)
            {
                float p1 = point2.X;
                float p2 = point1.X;

                point1.X = p1;
                point2.X = p2;
            }

            if (point1.Y > point2.Y)
            {
                float p1 = point2.Y;
                float p2 = point1.Y;

                point1.Y = p1;
                point2.Y = p2;
            }

            float width = point2.X - point1.X;
            float height = point2.Y - point1.Y;

            //float iterations = width > height ? width : height;

            for (int x = 0, y = 0; x <= width && y <= height;)
            {
                if (width > height)
                {
                    if (x >= width / height * y + 1)
                    {
                        y++;
                    }
                }
                else
                {
                    if (y >= height / width * x + 1)
                    {
                        x++;
                    }
                }

                float val;
                if (drawMode == DrawModes.Striped)
                {
                    val = y % 4;

                    ;
                }

                if (drawMode == DrawModes.Solid ||
                    (drawMode == DrawModes.Dotted && x % 4 == 0 && y % 4 == 0) ||
                    (drawMode == DrawModes.Striped && ((y % 3 != 0 && y % 4 != 0) ||
                                                        (x % 3 != 0 && x % 4 != 0))))
                {
                    Vector2 scale = new Vector2(_size);

                    _spriteBatch.Draw(TextureHandler.Pixel, new Vector2(point1.X + x, point1.Y + y) - scale / 2, null, _color, 0, Vector2.Zero, scale, SpriteEffects.None, 0.95f);
                }

                if (width > height) x++;
                else y++;
            }
        }
    }
}
