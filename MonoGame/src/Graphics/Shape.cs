﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace MonoGame.src.Graphics
{
    public class Shape
    {
        List<Vector2> _points;
        bool _fillContent;

        public Shape()
        {
            _points = new List<Vector2>();
            _fillContent = false;
        }

        public Shape(Transform transform) : this()
        {
            _points.Add(transform.Position);
            _points.Add(transform.Position + new Vector2(transform.Size.X, 0));
            _points.Add(transform.Position + transform.Size);
            _points.Add(transform.Position + new Vector2(0, transform.Size.Y));
            _points.Add(transform.Position);
        }

        public bool FillContent { get => _fillContent; set => _fillContent = value; }

        public void AddPoint(Vector2 point) => _points.Add(point);
        public void AddPoint(float x, float y) => _points.Add(new Vector2(x, y));
        public void AddPoints(List<Vector2> list) => _points.AddRange(list);
        public List<Vector2> Points { get => _points; }
    }
}
