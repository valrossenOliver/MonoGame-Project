﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace  MonoGame.src.Graphics
{
    public static class TextHandler
    {
        internal static void Initialize(ContentManager Content)
        {
            _default = Content.Load<SpriteFont>("Fonts/Default");
        }

        static SpriteFont _default;

        public static SpriteFont Default { get => _default; }

    }
}
