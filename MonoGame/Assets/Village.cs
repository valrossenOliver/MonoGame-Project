﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Assets.Objects;
using MonoGame.src;
using MonoGame.src.Components;
using MonoGame.src.Graphics;
using MonoGame.src.SceneManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame.Assets.Scenes
{
    class Village : Scene
    {
        Random random = new Random();
        TileMap _tileMap;

        public Village() : base("Village")
        {
            _tileMap = new TileMap("Village_Map", 32, 18, TextureHandler.Load("Grass"));

            var treeTexture = TextureHandler.Load("Tree");

            for (int i = 0; i < 250; i++)
            {
                var tree = GameObject.CreateNew("Tree", new Vector2(random.Next(0, 1900), random.Next(0, 1080 / 3 - 80)), new Vector2(29, 80), null);
                tree.AddComponent<Sprite>().Texture = treeTexture;

                tree.LayerDepth = 0.3f;

                AddObject(tree);
            }

            AddObjectsRange(new List<GameObject>()
            {
                _tileMap,
            });
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            foreach (var item in SceneObjects)
            {
                if (item.Enabled && item is IDrawable)
                {
                    item.Draw(gameTime, spriteBatch);
                }
            }

            base.Draw(gameTime, spriteBatch);
        }

        public override void PostUpdate(GameTime gameTime)
        {
            base.PostUpdate(gameTime);

            foreach (var item in SceneObjects)
            {
                if (item.Enabled)
                {
                    item.PostUpdate(gameTime);
                }
            }
        }

        public override void Reset()
        {
            base.Reset();
        }

        public override void Start()
        {
            base.Start();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            foreach (var item in SceneObjects)
            {
                if (item.Enabled)
                {
                    item.Update(gameTime);
                }
            }
        }
    }
}
