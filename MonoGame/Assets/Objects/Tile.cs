﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.src;
using MonoGame.src.Components;
using MonoGame.src.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame.Assets.Objects
{
    class Tile : GameObject
    {
        Sprite _texture;
        BoxCollider _boxCollider;

        public Tile(Texture2D texture = null, TileMap tileMap = null) : base(_name: "Tile", _parent: tileMap)
        {
            _texture = AddComponent<Sprite>();

            if (texture != null)
            {
                _texture.Texture = texture;
            }
            else
            {
                Color[] rawData = new Color[60 * 60];

                bool purple = true;
                for (int y = 0; y < 60; y++)
                {
                    if (y % 10 == 0)
                    {
                        purple = !purple;
                    }

                    for (int x = 0; x < 60; x++)
                    {
                        if (x % 10 == 0)
                        {
                            purple = !purple;
                        }

                        if (purple) rawData[y * 60 + x] = Color.Purple;
                        else rawData[y * 60 + x] = Color.Black;
                    }
                }

                texture = TextureHandler.Rectangle(60, 60, Color.Black);
                texture.SetData(rawData);

                _texture.Texture = texture;
            }

            _boxCollider = AddComponent<BoxCollider>();
            _boxCollider.Size = new Vector2(60, 60);
        }
    }
}
