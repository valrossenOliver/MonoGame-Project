﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MonoGame.src;
using MonoGame.src.Components;
using MonoGame.src.Graphics;
using MonoGame.src.SceneManagement;
using MonoGame.src.Time;
using System;
using System.Collections.Generic;

namespace MonoGame.Assets.Scenes
{
    class MainMenu : Scene
    {
        Random random;

        bool showPopup = false;
        Timer popupTimer = new Timer();
        Timer cloudTimer = new Timer();

        public MainMenu() : base("MainMenu")
        {
            random = new Random();
        }

        private void CloudTimer_OnTick(GameObject sender, Timer timer)
        {
            var cloud = GameObject.CreateNew("cloud",
                new Vector2(ScreenHandler.Width, random.Next(100, (int)ScreenHandler.Height - 200)), new Vector2(350, 100));
            cloud.AddComponent<Sprite>().Texture = content.Load<Texture2D>("Images/cloud");
            cloud.AddComponent<BoxCollider>();

            foreach (var item in GetObjectsByName("cloud"))
            {
                if (item != cloud)
                while (cloud.GetComponent<BoxCollider>().Touch(item.GetComponent<BoxCollider>()))
                {
                    var cloudPos = cloud.transform.Position;
                    cloudPos.Y = random.Next(0, (int)ScreenHandler.Height - 100);
                    cloud.transform.Position = cloudPos;
                }
            }

            AddObject(cloud);
        }

        private void PopupTimer_OnTick(GameObject sender, Timer timer)
        {
            popupTimer.Stop();
            popupTimer.Reset();
            showPopup = false;
        }

        private void btn_Start_Click(GameObject gameObject)
        {
            gameObject.GetComponent<AudioPlayer>().PlayEffect("paper_russtle");
            SceneManager.LoadScene("CharacterCreation");
        }

        private void btn_Help_Click(GameObject gameObject)
        {
            gameObject.GetComponent<AudioPlayer>().PlayEffect("paper_russtle");
            showPopup = true;
            popupTimer.Time = 5;
            popupTimer.Start();
        }

        private void btn_Exit_Click(GameObject gameObject)
        {
            game.Exit();
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            if (showPopup)
                ScreenHandler.Popup(spriteBatch, "Sorry!", "This part is not implemented yet. :(\n\n- Hamlet");

            var x = Keyboard.GetState().GetPressedKeys();

            for (int i = 0; i < x.Length; i++)
            {
                spriteBatch.DrawString(TextHandler.Default, x[i].ToString(), new Vector2(5, ScreenHandler.Height - 30 - i * TextHandler.Default.MeasureString(" ").Y), Color.White);
            }
        }

        public override void PostUpdate(GameTime gameTime)
        {
            base.PostUpdate(gameTime);
        }

        public override void Start()
        {
            Reset();
            base.Start();

            var menu = GetObjectByName("menu");
            
            if (menu != null && menu.HasComponent<AudioPlayer>())
                menu.GetComponent<AudioPlayer>().PlaySong("Lullaby");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            var clouds = GetObjectsByName("cloud");
            foreach (var cloud in clouds)
            {
                cloud.Move(new Vector2(-0.3f, 0));

                if (cloud.transform.Position.X + cloud.transform.Size.X < 0)
                {
                    cloud.Destroy();
                }
            }

            popupTimer.Update(gameTime);
            cloudTimer.Update(gameTime);
        }

        public override void Reset()
        {
            SceneObjects.Clear();

            #region Menu
            var buttonTexture = content.Load<Texture2D>("Images/paper_button");

            var menu = GameObject.CreateNew("menu", new Vector2(ScreenHandler.Width / 2, ScreenHandler.Height / 2));
            menu.AddComponent<AudioPlayer>();
            menu.GetComponent<AudioPlayer>().AddSong(content, "Lullaby");
            menu.AddComponent<Transision>().scene = this;

            var btn_Start = GameObject.CreateTemplate("btn_Start", GameObject.Templates.Button, _parent: menu);
            btn_Start.transform.localPosition = new Vector2(-100, -30 - 100);
            btn_Start.GetComponent<Text2D>().Text = "Start Game";
            btn_Start.GetComponent<Button>().SwapSetting.Normal = buttonTexture;
            btn_Start.AddComponent<AudioPlayer>().AddEffect(content, "paper_russtle");
            btn_Start.GetComponent<Button>().OnClick += btn_Start_Click;

            var btn_Help = GameObject.CreateTemplate("btn_Help", GameObject.Templates.Button, _parent: menu);
            btn_Help.transform.localPosition = new Vector2(-100, -30);
            btn_Help.GetComponent<Text2D>().Text = "Help\n(Not Implemented yet)";
            btn_Help.GetComponent<Button>().SwapSetting.Normal = buttonTexture;
            btn_Help.AddComponent<AudioPlayer>().AddEffect(content, "paper_russtle");
            btn_Help.GetComponent<Button>().OnClick += btn_Help_Click;

            var btn_Exit = GameObject.CreateTemplate("btn_Exit", GameObject.Templates.Button, _parent: menu);
            btn_Exit.transform.localPosition = new Vector2(-100, -30 + 100);
            btn_Exit.GetComponent<Text2D>().Text = "Exit";
            btn_Exit.GetComponent<Button>().SwapSetting.Normal = buttonTexture;
            btn_Exit.AddComponent<AudioPlayer>().AddEffect(content, "paper_russtle");
            btn_Exit.GetComponent<Button>().OnClick += btn_Exit_Click;

            popupTimer.OnTick += PopupTimer_OnTick;
            #endregion

            #region Create Clouds
            for (int i = 0; i < 5; i++)
            {
                var cloud = GameObject.CreateNew("cloud",
                new Vector2(random.Next(100, (int)ScreenHandler.Width - 200), random.Next(100, (int)ScreenHandler.Height - 200)), new Vector2(350, 100));
                cloud.AddComponent<Sprite>().Texture = content.Load<Texture2D>("Images/cloud");
                cloud.AddComponent<BoxCollider>();

                foreach (var item in GetObjectsByName("cloud"))
                {
                    if (item != cloud)
                        while (cloud.GetComponent<BoxCollider>().Touch(item.GetComponent<BoxCollider>()))
                        {
                            var cloudPos = cloud.transform.Position;
                            cloudPos = new Vector2(random.Next(100, (int)ScreenHandler.Width - 200), random.Next(100, (int)ScreenHandler.Height - 200));
                            cloud.transform.Position = cloudPos;
                        }
                }

                AddObject(cloud);
            }
            #endregion

            #region Create Sun
            var sun = GameObject.CreateNew("sun",
                    new Vector2(ScreenHandler.Width - 250, 50), new Vector2(100, 100));
            sun.AddComponent<Sprite>().Texture = TextureHandler.Circle(100, Color.White);
            sun.AddComponent<BoxCollider>();
            #endregion

            #region Create Stars
            for (int i = 0; i < 500; i++)
            {
                var star = GameObject.CreateNew("star",
                new Vector2(random.Next(10, (int)ScreenHandler.Width - 10), random.Next(10, (int)ScreenHandler.Height - 10)), new Vector2(1, 1));
                star.AddComponent<Sprite>().Texture = TextureHandler.Pixel;
                star.GetComponent<Sprite>().Color = Color.LightYellow;
                star.AddComponent<BoxCollider>();

                foreach (var item in GetObjectsByName("star"))
                {
                    if (item != star)
                        while (star.GetComponent<BoxCollider>().Touch(item.GetComponent<BoxCollider>()) || star.GetComponent<BoxCollider>().Touch(sun.GetComponent<BoxCollider>()))
                        {
                            var startPos = star.transform.Position;
                            startPos = new Vector2(random.Next(10, (int)ScreenHandler.Width - 10), random.Next(10, (int)ScreenHandler.Height - 10));
                            star.transform.Position = startPos;
                        }
                }

                AddObject(star);
            }
            #endregion

            #region Set up timers
            cloudTimer.Time = 15;
            cloudTimer.Start();
            CloudTimer_OnTick(null, cloudTimer);
            cloudTimer.OnTick += CloudTimer_OnTick;
            #endregion

            #region Add all objects to sceneObjects
            AddObjectsRange(new List<GameObject>()
            {
                menu, btn_Start, btn_Help, btn_Exit, sun
            }); 
            #endregion
        }
    }
}
