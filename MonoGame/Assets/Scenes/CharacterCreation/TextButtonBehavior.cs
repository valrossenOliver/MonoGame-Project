﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using MonoGame.src.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame.Assets.Scenes
{
    class TextButtonBehavior : GameBehaviour
    {
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (gameObject.GetComponent<Button>().IsActive)
            {
                gameObject.GetComponent<Text2D>().Color = Color.LightGray;
            }
            else if (gameObject.GetComponent<Button>().IsHovering)
            {
                gameObject.GetComponent<Text2D>().Color = Color.White;
            }
            else
            {
                gameObject.GetComponent<Text2D>().Color = Color.Black;
            }
        }
    }
}
